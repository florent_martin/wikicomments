####### directory names/paths
DATADIR = data
RAWDATADIR = $(DATADIR)/raw
SAMPLEDATADIR = $(DATADIR)/sample

####### file names/paths
RAWTRAINFILE = $(RAWDATADIR)/train.csv 
RAWSUBMISSIONFILE = $(RAWDATADIR)/sample_submission.csv
SAMPLESUBMISSIONFILE = $(SAMPLEDATADIR)/sample_submission.csv

####### kaggle api commands
# This is the name to give to kaggle to download the dataset
COMPETITION_NAME = jigsaw-toxic-comment-classification-challenge
DOWNLOAD_DATA = kaggle competitions download
DOWNLOAD_DATA_FLAGS = -c


# even if a file download_data is created, .PHONY  make will not consider downoad_data as a file, but just as a target
.PHONY: download_data

all: download_data 

############  DOWNLOAD THE DATA  ###########################

download_data : $(RAWTRAINFILE) $(SAMPLESUBMISSIONFILE)

# the following command requires to have installed the kagle-api 
# For that you should install it as explained in the readme file of https://github.com/Kaggle/kaggle-api
# and also configure a ~/.kaggle/kaggle.json file as explained in the readme file of https://github.com/Kaggle/kaggle-api

$(RAWTRAINFILE) : 
	$(DOWNLOAD_DATA) $(DOWNLOAD_DATA_FLAGS)  $(COMPETITION_NAME) -p $(RAWDATADIR)
	# $$ means $ in make
	for z in $(RAWDATADIR)/*.zip; do unzip $$z -d $(RAWDATADIR); done
         
$(SAMPLESUBMISSIONFILE) :
	mv $(RAWSUBMISSIONFILE) $(SAMPLESUBMISSIONFILE) 
