# Wikipedia Comments project


## Storing experiments

The module `./src/experiment/experiment.py` contains some utilities to store some experiments (from pipelines and gridsearch) and to access result of previous experiments.
To import the module from `./`, one can either enter
`import experiment.experiment`
or even 
`import experiment` (because the `__init__.py` file of `./experiment` imports the main classes from `./experiment/experiment.py`).

Then if `pipeline` is a pipeline that has been fitted, 
`experiment.PipelineExperiment(pipeline, X_test, y_test)` will store informations about this pipeline and the score obtained as a `.json` file in the directory `./Experiments/log/`.

Likewise, if `girdsearch` is a GridSearchCV that has been fitted, 
`experiment.GridsearchExperiment(gridsearch)` will store the experiment in the folder `./Experiments/log/`.

### Access the experiments

To acces the experiments one has to create a `Logger` object.
For that enter `logger = experiment.Logger()`.
Logger objects have a `to_df` method that returns a dataframe object of the experiments.
To get a Dataframe with the experiments enter 
`df_experiments = logger.to_df()`.

### Attributes of experiments
So far, all experiments have attributes `date`, `id` , `best_score_`, `best_params_`,  `named_steps`, `description`, `experiment_type`.
For `experiment.GridsearchExperiment`, there are other attributes.

### Where are the experiments stores?
The Logger class has a class variable `paths` which is a dictionary with integer keys and string values which are relative 
paths to directory where to store the experiments.
By default, all the experiments are stored in the directory `Logger.paths`, and by default, 
`Logger()` access the experiments in `Logger.paths`.

To see the dictionary of paths, enter 
`Logger.get_paths`.
To add a path to `Logger.paths` enter
`Logger.set_path(i,path)` and this will add the path `path` for the integer key `i`.

### Use manual path
If one does not want to use the dictionary `Logger.paths` it is possible to enter the paths manually.
For instance save an experiment with
`experiment.GridsearchExperiment(path='dir_path')` will save the experiment in the directory `'dir_path'`.
Similarly, to acces the experiments via Logger for an arbitrary directory, enter
`Logger(path='my_path')`.


