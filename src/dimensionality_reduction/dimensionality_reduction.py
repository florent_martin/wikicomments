from sklearn.base import BaseEstimator, TransformerMixin
import gensim.matutils
from gensim.models import LsiModel, LdaModel



def sparse_to_gensim(X_sparse):
    """Converts an numpy sparse array to gensim array with rows/columns switched"""
    X_gensim =  gensim.matutils.Sparse2Corpus(X_sparse, documents_columns=False)
    return X_gensim

def gensim_to_dense(X_gensim, num_terms):
    """Converts a gensim array to an numpy dense array with rows/columns switched"""
    X_numpy = gensim.matutils.corpus2dense(X_gensim, num_terms).transpose()
    return X_numpy

class GensimLSI(BaseEstimator, TransformerMixin):
    """
    Use gensim LSI
    Is fitted and transformed with a numpy array like input.
    The transform method outputs a numpy array like object.
    """
    
    def __init__(self, numtopics= 300):
        self.numtopics = numtopics
        return None
        
    def fit(self, X, y):
        X_gensim = sparse_to_gensim(X)
        self.model =LsiModel(X_gensim, num_topics=self.numtopics)
        return self
    
    def transform(self, X):
        X_gensim = sparse_to_gensim(X)
        Y_gensim = self.model[X_gensim]
        Y = gensim_to_dense(Y_gensim, self.numtopics)
        return Y 

    
