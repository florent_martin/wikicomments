import pickle   
import datetime
import os
import pandas as pd
import numpy as np
import json

# Encoder for numpyarrays
# from https://forum.freecadweb.org/viewtopic.php?t=3877
class NumPyArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist() # or map(int, obj)
        return json.JSONEncoder.default(self, obj)


class Experiment():  
    def __init__(self):
        self.date = str(datetime.datetime.now().replace(microsecond=0))
        self.id =  str(hex(abs(hash(self.date))))[2:]

    def get_dirpath(path):
        """Returns a directory path to save/load an experiment."""
        if type(path)==str:
            _path = path
        elif type(path)==int:
            _path = Logger.paths[path]
        else:
            raise Exception('The path p should be a string (directory path) or a valide integer.') 
        return _path
    
    def save_to_json(self, filename=None, path=0):
        """Save an experiment to a json file."""
        attributes = self.get_attributes()
        attribute_values = {attr: getattr(self, attr, None) for attr in attributes}
        if not filename:
             filename = 'experiment_' + self.id + '.json'
        _path = Experiment.get_dirpath(path)
        with open(_path + filename, 'w') as f:
            json.dump(attribute_values, f, cls=NumPyArrayEncoder, indent=2)
            
    def get_attributes(self):
        return self.attributes
    
    common_attributes = ['date', 'id' , 'best_score_','best_params_',  'named_steps', 'description', 'experiment_type']
    
    
class GridsearchExperiment(Experiment):
    def __init__(self, gridsearch, description='', save=True, path=0):
        super().__init__()
        self.pipeline = gridsearch.estimator
        self.named_steps = [ step for step in  self.pipeline.named_steps]
        #self.named_steps = {step: str(self.pipeline.named_steps[step]) for step in self.pipeline.named_steps}    
        self.best_params_ = gridsearch.best_params_
        self.best_score_ = float(gridsearch.best_score_)
        self.cv_results_ = gridsearch.cv_results_
        self.best_mean_train_score = self.cv_results_['mean_train_score'][np.argmin(self.cv_results_['rank_test_score'])]
        self.cv = gridsearch.cv
        self.description=description
        self.experiment_type='GridsearchCV'
        
        gridsearch_attributes = ['cv_results_', 'best_mean_train_score', 'cv']
        self.attributes= Experiment.common_attributes + gridsearch_attributes
        
        if save:
            self.save_to_json(path=path)
        
#     def to_series(self, attributes=['date', 'best_score_', 'named_steps','id','description']):
#         """
#         Converts an Experiment to Pandas series.
#         It assumes that the experiment comes from a gridsearch.
#         """
#         self_attributes =  list(filter(lambda attribute: hasattr(self, attribute), attributes ))
#         return pd.Series([ getattr(self, attribute) for attribute in self_attributes ], index=self_attributes)

#     @staticmethod
#     def save(self, filename=None, path=0):
#         """
#         Saves an experiment in a pickled file.
#         """
#         if not filename:
#             filename = 'experiment_' + self.id
#         _path = Experiment.get_dirpath(path)
#         with open(_path +  filename, 'w') as f:
#             pickle.dump(self, f)
            
#     def load(filename, path=0):
#         """Loads an experiment from a pickle object."""
#         _path = Experiment.get_dirpath(p)
#         with open(_path + filename, 'rb') as f:
#             experiment = pickle.load(f)
#         return experiment

   
# def save_as_experiment(gridsearch, description=''):
#     """Takes a gridsearch and saves it as an experiment"""
#     experiment = Experiment(gridsearch)
#     experiment.to_json()


class PipelineExperiment(Experiment):
    def __init__(self, pipeline, X_val, y_val, description='', save=True, path=0):
        super().__init__()
        self.pipeline = pipeline
        self.named_steps = [ step for step in  self.pipeline.named_steps]
        #self.named_steps = {step: str(self.pipeline.named_steps[step]) for step in self.pipeline.named_steps}       
        self.best_score_ = float(self.pipeline.score(X_val,y_val))
        self.best_params = self.pipeline.get_params()
        self.description=description
        self.experiment_type='Pipeline'
        pipeline_attributes = []
        self.attributes= Experiment.common_attributes + pipeline_attributes
        if save:
            self.save_to_json(path=path)
        print('score: {}'.format(self.best_score_))
        return None

class Logger():
    paths = {
        0 : '../Experiments/log/'
    }
    
    @classmethod
    def set_path(cls, idx, path):
        cls.paths[idx] = path
        return None
    
    @classmethod
    def get_paths(cls):
        return cls.paths
    
    def __init__(self, path=0):
        _path = Experiment.get_dirpath(path)
        filenames = os.listdir(_path)
        filenames = [filename for filename in filenames if filename.endswith('.json')] 
        experiments = []
        for filename in filenames:
            with open(_path + filename, 'r') as f:
                experiments.append(json.load(f))
        self.experiments = experiments
        
    def to_df(self):
        series_experiments = pd.Series(self.experiments)
        df_experiments = series_experiments.apply(pd.Series)
        df_experiments = pd.DataFrame(df_experiments, columns=Experiment.common_attributes + ['cv_results_', 'best_mean_train_score', 'cv'])
        df_experiments.set_index('id', inplace=True)
        df_experiments.sort_values('date', ascending=False, inplace = True)
        return df_experiments
