import re as re
import pandas as pd



def print_sample(samples, idx, samples_before=None, target=None):
    """Print sample with a given index"""
    print("--------sample with index: {} --------".format(idx))
    print(samples.loc[idx], "\n")
    if not (samples_before is None):
        print('--------------- Text before -----------------\n', samples_before.loc[idx], "\n")
    if not (target is None):
        print("TARGET: ", target.loc[idx])
    print("**"*30, "\n")
    
def print_random_samples(samples, num_samples=1, samples_before=None, \
                      target=None, mask=None, pattern=None):
    """Prints random samples from pd.Series.
    
    Parameters
    ----------
    samples :  pd.Series
        The series of samples you want to print  samples from.
        
    num_samples : int, default:1
        The number of samples to be printed.
        
    samples_before : pd.Series, None, default: None
       If not None, then the corresponding samples (i.e. those with the same index) 
       from texts will also be printed.
       The index of texts_before should be the same as the index of texts.
    
    target : pd.Series, None, default: None
        If not None, the samples of these Series will also be printed. 
        Index of target should be the same as for samples.
    
    mask : pd.Series, None, default: None
       If not None, it mask should be a masking bool Series, 
       and samples is replaced by samples.loc[mask].
    
    pattern : str, None, default: None
        If not None, pd.Series must be of type str, 
        must be a valid regular expression pattern, 
        then the samples will be selected from samples satisfying this pattern.
    
    """
    if not (mask is None):
        samples = samples.loc[mask]
    if not (pattern is None):
        pattern = re.compile(pattern)
        mask_pattern = samples.str.contains(pattern)
        samples = samples.loc[mask_pattern]
    if len(samples)<num_samples:
        print('WARNING: there are only {} samples satisfying your request. This is less that num_samples={}'.format(len(samples), num_samples))
    random_samples = samples.sample(num_samples)
    index = random_samples.index
    for idx in index:
        print("--------sample with index: {} --------".format(idx))
        print(samples.loc[idx], "\n")
        if not (samples_before is None):
            print('--------------- Text before -----------------\n', samples_before.loc[idx], "\n")
        if not (target is None):
            print("TARGET: ", target.loc[idx])
        print("**"*30, "\n")

        
def _get_pattern_statistics(texts, pattern, num_samples=None):
    """Computes the statistics on a given series of texts."""
    if not (num_samples is None): 
        num_samples = min(num_samples, len(texts))
        if num_samples<1:
            print('num_samples<1: Statistics on an empty set.')
            return None
        texts = texts.sample(num_samples)  
    pattern = re.compile(pattern)
    mask_pattern = texts.str.contains(pattern)
    num_texts = len(texts)
    num_tokens = texts.str.split().apply(len).sum()
    num_texts_with_pattern = texts.str.contains(pattern).sum()
    num_pattern_occurences = texts.str.count(pattern).sum()
    ratio_text_with_pattern_by_text = num_texts_with_pattern/num_texts
    ratio_pattern_by_token = num_pattern_occurences/num_tokens
    print("Ratio of texts which contain the pattern {}: {}".format(pattern, ratio_text_with_pattern_by_text))
    print("Ratio of tokens which contain the pattern {}: {}".format(pattern, ratio_pattern_by_token))
    return ratio_text_with_pattern_by_text, ratio_pattern_by_token  

def get_pattern_statistics(texts, pattern, target=None, mask=None, num_samples=None):
    """Prints and returns statistics appearance of a given regular expression pattern in a Series of texts.
    
    Parameters
    ----------
    texts : pd.Series of str
    
    pattern : str
        The pattern.
    
    target : pd.Series, None, default: None
        If not None, the statistics are conditioned by the value of target.
    
    mask : pd.Series of bool, None, default: None
       Masking Series to apply to texts.
       
    num_samples : int, None, default: None
        If not None, the statistics (on each group if target is applied) 
        are computed on maximum num_samples samples.
    
    Returns
    -------
    ratio of texts containing the pattern, ratio of tokens having the pattern. 
        If target is not None, returns a dictionary whose keys are the targets, 
        and values are the tuple of ratios for the corresponding target-groups.
    
    """
    
    if not (mask is None):
        texts = texts.loc[mask]
    if not (target is None):
        grouped = texts.groupby(target)
        ratio_dict={}
        for key, group in grouped:
            print("Statistics for target value = {}".format(key))
            ratios = _get_pattern_statistics(group, pattern, num_samples=num_samples)
            print('')
            ratio_dict[key]=ratios
        return ratio_dict
    else:
        return _get_pattern_statistics(texts, pattern)
    
