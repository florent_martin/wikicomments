import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Embedding
from keras.layers import LSTM
from keras import optimizers


class SimpleLSTM(BaseEstimator, TransformerMixin):
    def __init__(self, 
                input_dim, 
                 LSTM_units=128, 
                 batch_size=16, 
                 epochs=10, 
                 optimizer = optimizers.RMSprop(lr=0.001, clipnorm=1.),
                 dropout_rate=0.5
                 ):
        self.input_dim=input_dim
        self.LSTM_units=LSTM_units
        self.batch_size=batch_size
        self.epochs=epochs
        self.optimizer = optimizer
        self.dropout_rate = dropout_rate
        
    def fit(self, X, y=None):
        
        self.model = Sequential([
            LSTM(self.LSTM_units, input_shape=(None, self.input_dim)),
            Dropout(self.dropout_rate),
            Dense(1, activation='sigmoid')
        ])
        self.model.compile(loss='binary_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])
        self.model.fit(X, y, batch_size=self.batch_size, epochs=self.epochs)
        return self
        
    def predict_proba(self, X):
        probas = self.model.predict(X)
        return probas
    
    def predict(self, X):
        probas = self.predict_proba(X)
        predictions = probas>0.5
        return predictions
       
    
    def score(self, X, y):
        score = self.model.evaluate(X,y)
        return score
        