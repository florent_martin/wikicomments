import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras import regularizers
from sklearn.base import TransformerMixin, BaseEstimator
from keras.layers import Input, Layer


class FullyConnectedNetwork(BaseEstimator, TransformerMixin):
    """Fully Connected Network with Keras"""
    import keras
    from keras.models import Sequential
    from keras.layers import Dense, Activation, Dropout
    from keras import regularizers
    from keras.layers import Input, Layer
    
    def __init__(self, layers=[], epochs=1, batchsize=64, optimizer='adam', metrics=['accuracy'],\
                 activation='relu', validationsplit=.1, verbose=1, dropout=.5):
        self.layers = layers
        self.epochs = epochs
        self.batchsize = batchsize
        self.optimizer = optimizer
        self.metrics = metrics
        self.activation = activation
        self.validationsplit = validationsplit
        self.verbose = verbose
        self.dropout = dropout
                
    def fit(self, X, y):
        self.input_dim = X.shape[1]
        self.model = Sequential()
        self.model.add(Layer(input_shape=(self.input_dim,)))
        for h in self.layers:
        #     model.add(Dense(h,
        #                    kernel_regularizer=regularizers.l2(0.001)))
            self.model.add(Dense(h))
            self.model.add(Dropout(self.dropout))
            self.model.add(Activation(self.activation))
        self.model.add(Dense(1))
        self.model.add(Activation('sigmoid'))

        self.model.compile(optimizer=self.optimizer,
                      loss='binary_crossentropy',
                      metrics=self.metrics)
        self.model.fit(X, y, epochs=self.epochs, batch_size=self.batchsize, validation_split = self.validationsplit)
        return self
    
    def predict(self, X):
        pred = self.model.predict(X)
        return pred
    
    def predict_proba(self, X):
        pred = self.model.predict(X)
        return pred
    
    
    def score(self, X, y):
        score = self.model.score(X, y)
        return score
