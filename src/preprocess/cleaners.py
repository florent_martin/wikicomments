from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd

class TextCleaner(BaseEstimator, TransformerMixin):
    """Transformer that cleans texts.
    
    By default, it transforms some texts by replacing any sequence of non word characters and _ (in terms of regular expression: [\W_]) by a white space. It also removes non_ascii characters. Recall that \w is equivalent to [a-zA-Z0-9_] and \W is the negation of \w. The fit method takes a pd.Series (or array-like) of strings and returns a 'cleaned' pd.Series of strings.

    Parameters
    ----------
    replacements : list
        A list of pairs (pattern, replacement) used on the data via a sequence of X.str.replace(pattern, replacement), default: (r'\W+', ' ').
        
    remove_non_ascii : bool (default=False)
        If True, remove non ascii characters. Remark that with the default value of replacements, non_ascii characters are already removed.
    """
    
    def __init__(self, replacements=[ (r'\W+', ' ') ], remove_non_ascii=False):
        self.replacements = replacements
        self.remove_non_ascii = remove_non_ascii
    
    def fit(self, X=None, y=None):
        return self
    
    def transform(self, X):
        X = pd.Series(X)
        if self.remove_non_ascii:
            X = X.str.encode("ascii", errors="ignore")
            X = X.str.decode(encoding="utf-8")            
        for pattern, replacement in self.replacements:
            X = X.str.replace(pattern, replacement)
        return X


class CharacterCleaner(BaseEstimator, TransformerMixin):
    """
    Takes a pd.Series (or array-like) of strings and returns a cleaned pd.Series of strings.
    It removes non-ascii charcters.
    """
    def __init__(self):
        pass
    
    def fit(self, X=None, y=None):
        return self
    
    def transform(self, X):
        X = X.str.encode("ascii", errors="ignore")
        X = X.str.decode(encoding="utf-8")
        return X
    