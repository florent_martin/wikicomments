from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import re




class Tokenizer(BaseEstimator, TransformerMixin):
    """Tokenizer implemented as a scikit-learn transformer.
    
    The transform method takes as input a pd.Series of strings and outputs pd.Series of list of strings. The fit method does nothing.
    By default, it deletes punctuation symbols. If tokenize_punctuation is True, then it makes punctuation symbols at the end of words as a separate token.
    
    Parameters
    ----------
    remove_punctuation : bool (default=True)
        If True, punctuations symbols ,.!?;- are removed.
    
    lower_case : bool (default=True)
        If True, the words are put in lower case otherwise not. 
    
    tokenize_punctuation : bool (default=False)
        If True, punctuation symbols from 'punctuation_pattern' are extracted as a token.
        
    punctuation_pattern : str (default=r'([?!\.\,\;\-])')
        The pattern used to match punctuation symbols if tokenize_punctuation is True.
    
    stopwords : bool (default=False)
        If True remove stopwords. The list of stopwords is store in the attribute list_stopwords.
        
    list_stopwords : list, default: default_list_stopwords
        List of stopwords to use.
        
    pad_empty_text : bool (default=True)
        If True, empty texts are replaced by one single token: 'empty_text'.
    
    """
    def __init__(self, remove_punctuation=True,  \
                 lower_case=True, \
                 tokenize_punctuation=False, \
                 punctuation_pattern=r'([?!\.\,\;\-])' , \
                 stopwords=False, \
                 list_stopwords=None, \
                 pad_empty_text=True
                 ):
        self.remove_punctuation=remove_punctuation
        self.stopwords = stopwords
        if self.stopwords:
            if list_stopwords is None:
                self.list_stopwords = default_list_stopwords
            else:
                self.list_stopwords = list_stopwords
        self.tokenize_punctuation=tokenize_punctuation
        self.punctuation_pattern=punctuation_pattern
        self.lower_case = lower_case
        self.pad_empty_text=pad_empty_text
        
    def fit(self, X=None, y=None):
        if self.stopwords:
            if not (self.list_stopwords is None):
                self.list_stopwords = Tokenizer.default_list_stopwords
        return self
    
    def transform(self, X, y=None):
        X = pd.Series(X)
        if self.tokenize_punctuation: 
            X = X.str.replace(self.punctuation_pattern, r' \1 ')
        if self.remove_punctuation:
            X = X.str.replace(self.punctuation_pattern, r' ')
        if self.lower_case:
            X = X.str.lower()
        X = X.apply(lambda words: words.split())
        if self.stopwords:
            X = X.apply(lambda words: \
                        [word for word in words if word not in self.list_stopwords ])
        if self.pad_empty_text:
            X = X.apply(lambda l: l if l else ['empty_text'] )
        return X

default_list_stopwords = set(['all', "she'll", "don't", 'being', 'over', 'through', 
    'yourselves', 'its', 'before', "he's", "when's", "we've", 'had', 'should',
    "he'd", 'to', 'only', "there's", 'those', 'under', 'ours', 'has', 
    "haven't", 'do', 'them', 'his', "they'll", 'very', "who's", "they'd", 
    'cannot', "you've", 'they', 'not', 'during', 'yourself', 'him', 'nor', 
    "we'll", 'did', "they've", 'this', 'she', 'each', "won't", 'where', 
    "mustn't", "isn't", "i'll", "why's", 'because', "you'd", 'doing', 'some', 
    'up', 'are', 'further', 'ourselves', 'out', 'what', 'for', 'while', 
    "wasn't", 'does', "shouldn't", 'above', 'between', 'be', 'we', 'who', 
    "you're", 'were', 'here', 'hers', "aren't", 'by', 'both', 'about', 'would', 
    'of', 'could', 'against', "i'd", "weren't", "i'm", 'or', "can't", 'own', 
    'into', 'whom', 'down', "hadn't", "couldn't", 'your', "doesn't", 'from', 
    "how's", 'her', 'their', "it's", 'there', 'been', 'why', 'few', 'too', 
    'themselves', 'was', 'until', 'more', 'himself', "where's", "i've", 'with', 
    "didn't", "what's", 'but', 'herself', 'than', "here's", 'he', 'me', 
    "they're", 'myself', 'these', "hasn't", 'below', 'ought', 'theirs', 'my', 
    "wouldn't", "we'd", 'and', 'then', 'is', 'am', 'it', 'an', 'as', 'itself', 
    'at', 'have', 'in', 'any', 'if', 'again', 'no', 'that', 'when', 'same', 
    'how', 'other', 'which', 'you', "shan't", 'our', 'after', "let's", 'most', 
    'such', 'on', "he'll", 'a', 'off', 'i', "she'd", 'yours', "you'll", 'so', 
    "we're", "she's", 'the', "that's", 'having', 'once'])    

class SeparatePunctuation(BaseEstimator, TransformerMixin):
    """Takes a pd.Series (or array-like) of strings and returns a pd.Series of strings where white spaces are added before and after punctuation"""
    def __init__(self, punctuation_regex= r"""([\(\)\?\!\.\,\-\/]+)"""):
        self.punctuation_regex=punctuation_regex
    
    def fit(self, X=None, y=None):
        return self
    
    def transform(self, X):
        X = pd.Series(X)
        pattern = re.compile(self.punctuation_regex)
        X = X.str.replace(pattern, r' \1 ')
        X = X.apply(lambda text: " ".join(text.split() ) )
        return X

class Text2SpacyDoc(BaseEstimator, TransformerMixin):
    """Transformer that transform strings in spacy.Doc objects."""
    import spacy
    def __init__(self, model=spacy.load('en_core_web_sm')):
        self.model=model
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(self.model)
        return X

def replace_ner_spacy(doc, remove_entity=False):
    """Takes a spacy doc, and replace all NER from spacy by their tag."""
    import spacy
    text=doc.text
    return_text=""
    pos=0
    for ent in doc.ents:
        if ent.text == ' ': continue
        return_text+=text[pos:ent.start_char]
        entity = ent.text
        return_text+= "ner"+ent.label_.lower() + " "
        pos = ent.end_char+1
    return_text += text[pos:]
    return return_text

class NerTaggerSpacy(BaseEstimator, TransformerMixin):
    """Transformer that transforms spacy.Doc objects to strings where NER are tagged."""
    import spacy
    def __init__(self, remove_entity=False):
        self.remove_entity=remove_entity
    
    def fit(self, X, y=None):
        return self
    
    def transform(self,X ,y=None):
        X = X.apply(replace_ner_spacy)
        return X
