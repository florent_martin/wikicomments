# this file contains various taggers to make some preprocessing
# the function tag_all takes as input a series of strings (the comments) and returns a series of strings 

import re
import calendar
from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd

class Tagger(BaseEstimator, TransformerMixin):
    """Takes a pd.Series (or array-like) of strings and returns a cleaned pd.Series of strings.
    
    Parameters
    ----------
    remove_from_string : bool (default=False)
        If True, all tagged elements are removed from the strings.
    
    verbose : bool (default=False)
        If True, prints when each tagger starts.
    """
    def __init__(self, remove_from_string=False, verbose=False):
        self.verbose=verbose
        self.remove_from_string=remove_from_string
    # taggers is a dictionary of all our taggers
    # The reason why I don't want to create simply a dictionary of the taggers and maintain a separte list of the tagger names (which are the keys of the dictionary taggers) is that at then end, the order in which you apply the taggers matter, hence having the list tagger_names allows to keep track of that order    
    taggers = {}
    sequence_of_taggers = []
    
    @classmethod
    def add(cls, tagger_name, pattern_list, tag=None, add_to_sequence=True, overwrite=True):
        """Adds a tagger to the dictionary taggers and the list sequence_of_taggers."""
        if tag==None: tag = ' tag_' + tagger_name + ' '
        pattern_list = [re.compile(pattern) for pattern in pattern_list]
        
        def tagger_function(X, tag=tag, remove_from_string=False):
            if remove_from_string:
                tag = ' '
            for pattern in pattern_list:
                X = X.str.replace(pattern, tag) 
            return X
        
        if tagger_name in Tagger.taggers and not overwrite:
            raise Exception("The tagger_name {} is already in Tagger.taggers. Use flag overwrite=True in Tagger.add if you want to overwrite".format(tagger_name))
        else:
            Tagger.taggers[tagger_name] = {
                'name': tagger_name, 
                'function': tagger_function, 
                'tag': tag
            }
        
        if add_to_sequence:
            Tagger.sequence_of_taggers.append(tagger_name)
        
    def fit(self, X=None, y=None):
        return self
    
    def transform(self, X):
        X = pd.Series(X)
        if self.verbose: print('Start Tagger')
        
        for tagger in self.sequence_of_taggers:
            if self.verbose: print(tagger['name'])
            X = Tagger.taggers[tagger]['function'](X, remove_from_string=self.remove_from_string)
        return X

############################################################################        
## TRAILING SYMBOLS AT THE BEGGINING AND END OF A TEXT                ######
############################################################################
regexs_trailing = [
    r"""^['"\n\s\#]*""",
    r"""['"\n\s#]*$"""
]
Tagger.add('trailing', regexs_trailing, tag='')

###################
## IP addresses  ##
###################
regexs_IP = [
    r'(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})'
]
Tagger.add('IP_address', regexs_IP)
#####################
## HTTP AND EMAILS ##
#####################
regexs_http = [
    # http://....
    r'(http|www)://\S*',
    # wwwfuiue.??
    r'www\S*\.\w{2,3}',
    # ...@jfdkjf.fjk
    r'\S*\w\@\w+\.\S+',
]
Tagger.add('http_email', regexs_http)

######################
#### IMAGES     ######
######################
regexs_image = [
    r'Image:(\s*\S*){1,9}?.(pdf|jpg|JPG|GIF|gif|jpeg|png|PNG|svg|m4a\?)[\w\|\=]*'
]
Tagger.add('image', regexs_image)

#####################
## DATE AND TIME ####
#####################
months_Jan  = [month[:3] for month in calendar.month_name[1:]]
#months_Jan = ['Jan', 'Feb',...,'Dec']
months_jan  = [month[:3].lower() for month in calendar.month_name[1:]]
#months_jan = ['jan','feb',...'dec']
months_January = calendar.month_name[1:]
#months_January = ['January', 'February', ..., 'December']
months_january = [month.lower() for month in calendar.month_name[1:]]
#months_january = ['january',...,'december']
months_regex = "|".join(months_January + months_Jan + months_january + months_jan)
# months_regex = 'Jan|Feb|...|January...|December'

date_time_regexs = [
#date in format (07:08,) 29 2012  May 2012 (UTC)
r'(\d\d:\d\d,)?\s*\d{1,2} \d\d\d\d\s*('+ months_regex + r')\s*((\d{4})?\s*\(UTC\))?',
#date in format (07:08,) 2004 May 24 (UTC)
r'(\d\d:\d\d,)?\s*\d\d\d\d\s*('+ months_regex + r')\s*\d{1,2}\s*(\(UTC\))?',
# date in format (17:01,) Nov(ember) 16, 2004 (UTC)
r'(\d\d:\d\d,)?\s*('+ months_regex + r')\s*\d{1,2}(,\s*\d\d\d\d)\s*(\(UTC\))?',
# date in format (17:01,) 16(th of.,) Nov(ember), 2004 (UTC)
r'(\d\d:\d\d,)?\s*\d{1,2}\s*(th|th\s*of|of|\.|\,)?\s*('+ months_regex + r')(,?\s*\d\d\d\d)?\s*(\(UTC\))?',
#date in format January (7,) 2011 (UTC)
r'('+months_regex+r')\s*(\d{1,2},?)?\s*\d{4}\s*(\(UTC\))?',
# date in format 2008 (UTC)
r'\d{4}\s*\(UTC\)',
#date in format 25/08/06
r'\d{1,2}/\d{2}/\d{2}(\d{2})?',
# date as 03:28, 27 at the end of a comment
r'\d\d:\d\d,\s* \d{1,}\s*$',
#date of the form 10:40, 27 S EOF
r'\d\d:\d\d,\s* \d{1,2}\s*\w{1,3}\s*$',
#time of the form 10:40 
r'\d\d:\d\d'
]

Tagger.add('date_time', date_time_regexs )

##############
## USERNAMES #
###############
username_regexs = [
    re.compile(r'User:(\s*(\S+))?', re.I)
]
Tagger.add('username', username_regexs)

##########
## TAB ###
##########
tab_regexs = [
    r'TAB'
]
Tagger.add('tab', tab_regexs)

##############
#### CSS  ###
##############
TAG_CSS = 'tag_css'
css_regexs=[
# {| ... \n
r'\{\|.*\n',
#...style="...
r'(class|style|align|width|rowspan|colspan|bgcolor)=\"\S*',
# ..text-align:..
r'(border|text-align|background-color|vertical-align|color|font-size):\S*',
# | TAG_CSS . |
r'(\|[^\n]*)?'+TAG_CSS+r'[^\n]*\|',
# colorhex
r'\#[\dA-F]{6}\S*',
# coup de grace: TAG_CSS ... TAG_CSS
TAG_CSS+r'[^\n]'+TAG_CSS
]

Tagger.add('css', css_regexs, tag=TAG_CSS)

#############
## TALK ####
############
talk_regexs = [
r'\(talk\)',
r'Talk:',
r'[uU]ser[\s_]talk:',
r'[wW]ikipedia[\s_]talk:',
r'\S*_talk:',
r'talk:',
r'Talk Page',
r'talk page'
]
Tagger.add('talk', talk_regexs)

################
##  WIKIPEDIA ##
################
wiki_regexs = [
r'WP:\S+',
r'[wW]ikipedia:(?=\S)'
]
Tagger.add('wikipedia', wiki_regexs)

###############
## TILDES ####
##############
tildes_regexs = [
    r'\(~~~~\)',
    r'~~~~'
]
Tagger.add('tildes', tildes_regexs)

##################
## NUMBERS   #####
##################
numbers_regexs = [
r'\d+'
]
Tagger.add('numbers', numbers_regexs)

######################
## MISCELLANEOUS #####
#####################
misc_regexs = [
r"REDIRECT (User )?talk:", 
r'Help:',
r'Template:',
r'Category:'
]
Tagger.add('miscellaneous', misc_regexs)

##############
## QUOTES  ###
##############
# quotes_regexs = [
#     r"""["']+"""
# ]
# Tagger.add('quotes', quotes_regexs) 