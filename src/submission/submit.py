#!/usr/bin/env python

import sys
import os

input_path = sys.argv[1]

command0 ="kaggle competitions submit -c jigsaw-toxic-comment-classification-challenge -f "
command1 = input_path
command2 = " -m 'submission message'"
command = command0 + command1 + command2

os.system(command)
