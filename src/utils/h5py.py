import h5py
import math
import numpy as np
from keras.utils import Sequence

class Sequence_h5py_shuffle(Sequence):
    """Keras generator that converts a h5py file to a batch generator and shuffles it.
    
    Parameters
    ----------
    
    filename : str
        Then path of the hdf5 file.
    batch_size : int
    
    X : str 
        The name of the dataset in filename for the feature values. Default 'X'.
        
    y : str
        The name of the dataset in filename for the target values. Default 'y'.
        
    """
    def __init__(self, filename, batch_size, X='X', y ='y'):
        self.filename = filename
        self.batch_size = batch_size
        self.X = X
        self.y = y
        with h5py.File(self.filename, 'r') as f:
            dataset = f[self.X]
            self.length = len(dataset)
        self.steps_per_epoch = self.__len__()
        self.num_epochs = 0
        self.initialize_seed()
        
        
        
    def __len__(self):
        with h5py.File(self.filename, 'r') as f:
            dataset = f[self.X]
            steps_per_epoch = math.ceil(len(dataset) / self.batch_size)
        return steps_per_epoch
    
    def initialize_seed(self):
        self.index=0
        self.num_epochs+=1
        self.seed = np.random.permutation(self.length)
    
    def __getitem__(self, index):
        indices = sorted(list(self.seed[self.index : self.index+self.batch_size]))
        if self.index + self.batch_size >= self.length:
            self.initialize_seed()
        self.index += self.batch_size
        with h5py.File(self.filename, 'r') as f:
                X, y = f[self.X][ indices ] , f[self.y][ indices ]
                return X, y  

def to_hdf5_pipeline(X, y, filename, fitted_pipeline, chuncksize, features_shape, dtype='float32', X_name='X', y_name='y'):
    """Saves pipelined version of X (by this we mean the result of the fitted_pipeline applied to X) 
    and y as an hdf5 file with path filename, using chuncksize.
    Overwrites the datasets if they already exist.
    
    Parameters
    ----------
    X : np-array
    
    y : np-array
    
    fitted_pipeline: a scikit-learn Pipeline
    
    chuncksize : int or float
       If float, size of the chuncks, if float, ratio fo the chuncksize compared to the length of X.
    
    feature_shape : tuple of int
        the shape of the returned array for each sample in X.

    X_name : str
        The name of the dataset to be created for X, default 'X'.
        
    y_name : str
        The name of the dataset to be created for y, default 'y'.
       
    """
    assert(len(X)==len(y))
    length = len(X)
    if isinstance(chuncksize, float):
        chuncksize = math.ceil(chuncksize * length)
    num_chuncks = math.ceil(length/chuncksize)
    
    with h5py.File(filename, 'a') as f:
        if X_name in f.keys():
            del f[X_name]
        f.create_dataset(X_name, shape = (len(X), *features_shape), dtype=dtype )
        if y_name in f.keys():
            del f[y_name]
        f.create_dataset(y_name, shape = y.shape, dtype=dtype)

        arrays = np.array_split(X, num_chuncks)
        ys = np.array_split(y, num_chuncks)
        pos = 0
        for arr in arrays:
            arr = fitted_pipeline.transform(arr)
            f[X_name][pos:pos + len(arr)] = arr
            pos+=len(arr)
        pos=0
        for arr in ys:
            f[y_name][pos:pos + len(arr)] = arr
            pos+=len(arr)
    return None

def shuffle_hdf5(filename, name, filename_shuffled, name_shuffled, axis=0):
    """Shuffles a dataset from an hdf5 dataset.
    
    Parameters
    ----------
    filename : str
        Path of the hdf5 file.
    
    name : str
        Name of the dataset in the hdf5 file.
        
    
    """
    import h5py
    import random
    
    with h5py.File(filename, 'r') as f:
        dataset = f[name]
        shape = dataset.shape
        dtype = dataset.dtype
        length = shape[0]
        indices = list(range(length))
        random.shuffle(indices)
        with h5py.File(filename_shuffled, 'w') as f_shuffled:
            dataset_shuffled = f_shuffled.create_dataset(name_shuffled, dtype=dtype, shape=shape)
            for i,j in enumerate(indices):
                dataset_shuffled[i]=dataset[j]    
    return None
