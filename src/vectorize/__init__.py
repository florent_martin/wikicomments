from .vectorize import GensimWord2Vec, Averager, Padding, TokenToTokenFeatured, \
LowerCaseFeatured, RemoveDuplicatesFeatured, GensimWord2VecFeatured, TokenLengthFeatured, GetToken, GetFeatures, QuotedTokenFeatured

from .multiindex import CastTypesMultiindex, DuplicateMultiindex, GensimWord2VecMultiindex, \
GetFeaturesMultiindex, GetTokenMultiindex, LengthMultiindex, LowerCaseMultiindex, \
PadderMultiindex, QuotesMultiindex, SeriesOfListOfTokens2MultiindexSeriesOfTokens

import h5py
import math

def to_hdf5(X, y, filename, fitted_pipeline, chuncksize, features_shape, dtype='float32'):
    """Saves pipelined version of X and y as an hdf5 file with path filename, using chuncksize."""
    assert(len(X)==len(y))
    length = len(X)
    sections = math.ceil(length/chuncksize)
    
    f = h5py.File(filename, 'w')
    f.create_dataset('X', shape = (len(X), *features_shape), dtype=dtype )
    f.create_dataset('y', shape = y.shape, dtype=dtype)
    
    arrays = np.array_split(X, sections)
    ys = np.array_split(y, sections)
    pos = 0
    for arr in arrays:
        arr = fitted_pipeline.transform(arr)
        f['X'][pos:pos + len(arr)] = arr
        pos+=len(arr)
    pos=0
    for arr in ys:
        f['y'][pos:pos + len(arr)] = arr
        pos+=len(arr)
    f.close()
    return None