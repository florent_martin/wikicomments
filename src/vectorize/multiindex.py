"""Multiindex module.

This module provides Transformers following scikit-learn API to add some features to tokens. 
A pd.Series 'texts' of str can be converted in a pd.Series of tokens 'tokens_multiindex' (str again) with multiindex with first level the index of the text in 'texts' and second index the position of the token in the given text.
Then this pd.Series is converted to a DataFrame where one column is the token, and other columns are features to be added.
With this token_multiindex representation, many operations are fast.
"""

from gensim.models.word2vec import Word2Vec
import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from nltk.corpus import words
import re
import logging
import itertools

TOKEN = 'token'
TAG_UNKNOWN='tag_unknown'


class SeriesOfListOfTokens2MultiindexSeriesOfTokens(BaseEstimator, TransformerMixin):
    """Transformer to convert pd.Series of lists of tokens into a pd.DataFrome with one single columns 'token' with multiindex: first level is the index of the sentence, second index is the position of the token in the sentence.
    ALL LIST OF TOKENS SHOULD BE NONEMPTY, OTHERWISE EMPTY LIST OF TOKENS DISAPPEAR.
    
    It follows scikit-learn transformer API.
    """
    
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)    
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        self.logger.info('start')
        tokens = list(itertools.chain(*X))
        text_idxs_chain = ( [idx]*len(tokens) for idx, tokens in X.iteritems()  )
        text_idxs = list(itertools.chain(*text_idxs_chain))
        position_chain = [ range(len(tokens)) for tokens in X ]
        position = list(itertools.chain(*position_chain))
        multiindex = pd.MultiIndex.from_arrays([text_idxs, position])
        df = pd.DataFrame({TOKEN: tokens}, index = multiindex)
        self.logger.info('end transform')
        return df

class CastTypesMultiindex(BaseEstimator, TransformerMixin):
    """Transformer that casts floats to a given dtype."""
    def __init__(self, dtype = 'float32'):
        self.dtype=dtype
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        dtypes = X.dtypes
        dict_types = {col: self.dtype for col in X.columns if dtypes.loc[col].kind=='f'}
        X = X.astype(dtype = dict_types)
        return X

class GetFeaturesMultiindex(BaseEstimator, TransformerMixin):
    """Transformer that transform list of multiindex (token, features) into features."""
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        columns = list(X.columns)
        columns.remove(TOKEN)
        X = X.loc[:,columns]
        return X

class GetTokenMultiindex(BaseEstimator, TransformerMixin):
    """Transformer that transform list of multiindex (token, features) into features."""
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        column_token = 'token'
        tokens=X.loc[:,column_token]
        return tokens

class PadderMultiindex(BaseEstimator, TransformerMixin):
    """Transformer
    
    Takes the multiindex dataframe of features and returns a 3-d nparray of sequence of length max_len of vectors.
    
    Parameters
    ----------
    max_len : int 
        Length of the sequence after padding.
        
    dtype : str (default='float32')
       The dtype returned.
    
    fit_only : bool (default=False)
       If False, the transformer has a normal behaviour, if True, it is fitted only but transform acts as the identity function.
    
    """
    def __init__(self, max_len, dtype='float32', fit_only=False):
        self.max_len = max_len
        self.dtype=dtype
        self.fit_only = fit_only
        self.logger = logging.getLogger(self.__class__.__name__)
        
    def fit(self, X=None, y=None):
        return self
    
    def transform(self, X, y=None, token_column = TOKEN):
        if self.fit_only:
            return X
        n_samples = len(X.index.levels[0])
        n_features = X.shape[1]
        return_array = np.zeros((n_samples, self.max_len, n_features), dtype=self.dtype)
        labels = X.index.labels[0]
        levels = X.index.levels[0]
        labels = [levels[i] for i in labels]
        order_of_index = {}
        set_index = set()
        j=0
        for l in labels:
            if not l in set_index:
                order_of_index[l] = j
                j+=1
                set_index.add(l)
        for idx, tokens_vectorized in X.groupby(level=0):
                truncated_length = min(self.max_len, len(tokens_vectorized))
                vectors = tokens_vectorized.iloc[:truncated_length]
                i = order_of_index[idx]
                return_array[i, :truncated_length] = vectors
        return return_array
    
class LowerCaseMultiindex(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None, token_column=TOKEN):
        """X is a multiindex dataframe with a column token"""
        tokens = X.loc[:, token_column]
        first_letter=tokens.str.get(0)
        is_first_letter_cap_bool = first_letter.str.isupper()
        is_first_letter_cap = is_first_letter_cap_bool.astype('float')
        pattern = re.compile(r'[A-Z]')
        num_cap_letters = tokens.str.count(pattern)
        num_non_first_cap_letters = num_cap_letters - is_first_letter_cap
        tokens_lower = tokens.str.lower()
        assign_dict = {
            token_column: tokens_lower,
            'start_cap': is_first_letter_cap,
            'num_caps': num_non_first_cap_letters
        }
        X = X.assign(**assign_dict)
        return X

class QuotesMultiindex(BaseEstimator, TransformerMixin):
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None, token_column=TOKEN):
        tokens = X.loc[:,token_column]
        has_quotes = tokens.str.match(r"""^['"]+\w+['"]+$""")
        tokens_with_quotes0 = tokens[has_quotes]
        pattern = re.compile(r"""^['"]+""")
        tokens_with_quotes1 = tokens_with_quotes0.str.replace(pattern, '')
        pattern = re.compile(r"""['"]+$""")
        tokens_with_quotes2 = tokens_with_quotes1.str.replace(pattern, '')
        feature_quote = has_quotes.astype('float')
        new_tokens = tokens.where(~has_quotes, tokens_with_quotes2)
        assign_dict = {
            token_column: new_tokens,
            'quoted': feature_quote
        }
        X = X.assign(**assign_dict)
        return X

words_set = set(words.words())

class DuplicateMultiindex(BaseEstimator, TransformerMixin):
    """Transformer which takes list of (token, features) and remove consecutive duplicatecharacters 
    in token, and adds a feature to count the number of duplicates."""
    def __init__(self):
        self.vocab=words_set
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None, token_column=TOKEN):
        tokens = X.loc[:, token_column]
        mask_in_vocab = tokens.apply(self.vocab.__contains__)
        tokens_not_in_vocab = tokens[~mask_in_vocab]
        tokens_in_vocab = tokens[mask_in_vocab]
        pattern = r'(.)\1+'
        tokens_without_duplicates = tokens_not_in_vocab.str.replace(pattern, r'\1')
        duplicate_feature = tokens_not_in_vocab.str.len() - tokens_without_duplicates.str.len()
        duplicate_feature = duplicate_feature.astype('float')
        duplicate_feature2 = pd.Series(data=0., index=tokens.index)
        duplicate_feature2 = duplicate_feature2.where(mask_in_vocab, duplicate_feature)
        new_tokens = tokens.where(mask_in_vocab, tokens_without_duplicates)
        assign_dict = {
            token_column: new_tokens,
            'duplicates': duplicate_feature2
        }
        X = X.assign(**assign_dict)
        return X

class LengthMultiindex(BaseEstimator, TransformerMixin):
    """Transformer which takes list of (token, features) and remove consecutive duplicatecharacters 
    in token, and adds a feature to count the number of duplicates."""
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None, token_column=TOKEN):
        tokens = X.loc[:,token_column]
        length = tokens.str.len()
        assign_dict = {
            'length': length
        }
        X = X.assign(**assign_dict)
        return X
    
    
class GensimWord2VecMultiindex(BaseEstimator, TransformerMixin):    
    """
    Gensim word2vec.
    Transform and fit take as input an series of (token, features).
    The method transform returns a pd.Series of  list of  (token, features) where 
    word2vec vectors have been added to features.
    
    Parameters
    ----------
    size : int
        Size of the vector representation, default 200.
        
    window : int
        Size of the window, default 5.
        
    mincount : int
        Minimum occurence of a word for it to appear in the dictionnary. Default 5.
        
    sample : float
      
    sg : int
       Type of algorithm for word2vec: 0 for skipgram 1 for continuous bag of words.
    
    keep_unkwon : bool
    
    add_features : bool
        If False, then the transform method will not add the features from word2vec. This is usefull in a Pipeline, if there are some additional transformers in the Pipeline after word2vec, and we do want to fit the pipeline, but we do not have enough RAM to transform all the input. 
    """
    def __init__(self, size=100, window=5, mincount=5, sample=1e-3, sg=0, keep_not_in_vocab=False, fit_only=False):
        self.size = size
        self.window = window
        self.mincount = mincount
        self.sample = sample
        self.sg=sg
        self.keep_not_in_vocab = keep_not_in_vocab
        self.fit_only = fit_only
        self.logger=logging.getLogger(self.__class__.__name__)

    def fit(self, X, y=None, token_column = TOKEN):
        self.logger.info('Start to create `sentences`')   
        tokens = X.loc[:,token_column ]
        grouped = tokens.groupby(level=0)
        sentences = [ list(sentence.values) for _ , sentence  in grouped  ]
        self.logger.info('`sentences` created')
        self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
                              min_count=self.mincount, sample=self.sample, 
                              sg=self.sg,
                              workers=3)
        return self
        
    def transform(self, X, y=None, token_column = TOKEN):
        if self.fit_only:
            return X                
        tokens = X.loc[:, token_column ]
        num_tokens = len(tokens)
        old_columns = list(X.columns.drop(token_column))
        num_old_columns = len(old_columns)
        word2vec_columns = ['w2v' + str(i) for i in range(self.size) ]
        new_columns =  word2vec_columns + old_columns 
        num_new_columns = len(new_columns)
        
        # old features
        old_features = X[old_columns].values
        
        # features to be returned as np.array
        features = np.zeros( (num_tokens, num_new_columns), dtype='float32' )

        # word2vec features as np.array
        word2vec_features = np.zeros( (num_tokens, self.size), dtype='float32')
        if self.keep_not_in_vocab:
            # for loop over all tokens
            for (i,token) in enumerate(tokens):
                if token in self.model.wv:
                    word2vec_features[i] = self.model.wv[token]
            features[:, :self.size] = word2vec_features[:]
            features[:, self.size:] = old_features[:]
            df = pd.DataFrame(features, index = X.index.copy(), columns=new_columns)
            df['token'] = tokens
            return df   
        else:    
            set_sentence_index = set()
            list_tokens_index = []
            labels = X.index.labels[0]
            levels = X.index.levels[0]
            # index_of_sentence is the list of sentence indexes for each token
            index_of_sentence = [levels[i] for i in labels]
            for (i, ( (_, pos),token ) ) in enumerate(tokens.iteritems()):
                if token in self.model.wv:
                    word2vec_features[i] = self.model.wv[token]
                    if index_of_sentence[i] not in set_sentence_index: 
                        set_sentence_index.add(index_of_sentence[i])
                    list_tokens_index.append(i)
                else:
                    if index_of_sentence[i] not in set_sentence_index: 
                        set_sentence_index.add(index_of_sentence[i])
                        list_tokens_index.append(i)


            features[:, :self.size] = word2vec_features[:]
            features[:, self.size:] = old_features[:]
            df = pd.DataFrame(features, index = X.index.copy(), columns=new_columns)
            df['token'] = tokens
            df = df.iloc[list_tokens_index]
            return df    
# class GensimWord2VecMultiindex(BaseEstimator, TransformerMixin):    
#     """
#     Gensim word2vec.
#     Transform and fit take as input an series of (token, features).
#     The method transform returns a pd.Series of  list of  (token, features) where 
#     word2vec vectors have been added to features.
    
#     Parameters
#     ----------
#     size : int
#         Size of the vector representation, default 200.
        
#     window : int
#         Size of the window, default 5.
        
#     mincount : int
#         Minimum occurence of a word for it to appear in the dictionnary. Default 5.
        
#     sample : float
      
#     sg : int
#        Type of algorithm for word2vec: 0 for skipgram 1 for continuous bag of words.
    
#     keep_unkwon : bool
    
#     add_features : bool
#         If False, then the transform method will not add the features from word2vec. This is usefull in a Pipeline, if there are some additional transformers in the Pipeline after word2vec, and we do want to fit the pipeline, but we do not have enough RAM to transform all the input. 
#     """
#     def __init__(self, size=100, window=5, mincount=5, sample=1e-3, sg=0, keep_unknown=False, add_features=True):
#         self.size = size
#         self.window = window
#         self.mincount = mincount
#         self.sample = sample
#         self.sg=sg
#         self.keep_unknown=keep_unknown
#         self.add_features = add_features
#         self.logger=logging.getLogger(self.__class__.__name__)

#     def fit(self, X, y=None, token_column = TOKEN):
#         self.logger.info('Start to create `sentences`')   
#         tokens = X.loc[:,token_column ]
#         grouped = tokens.groupby(level=0)
#         sentences = [ list(sentence.values) for _ , sentence  in grouped  ]
#         self.logger.info('`sentences` created')
#         self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                               min_count=self.mincount, sample=self.sample, 
#                               sg=self.sg,
#                               workers=3)
# #         if self.keep_unknown:
# #             tagged_tokens = tokens.where(tokens.apply(self.model.wv.__contains__), TAG_UNKNOWN)
# #             index_level = tagged_tokens.index.levels[0]
# #             sentences = [ list(tagged_tokens.loc[idx]) for idx in index_level  ]
# #             self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
# #                                   min_count=self.mincount, sample=self.sample, 
# #                                   sg=self.sg,
# #                                   workers=2)
#         #self.model.init_sims(replace=True)
#         return self
        
#     def transform(self, X, y=None, token_column = TOKEN):
#         if not self.add_features:
#             return X                
#         tokens = X.loc[:, token_column ]
#         num_tokens = len(tokens)
#         old_columns = list(X.columns.drop(token_column))
#         num_old_columns = len(old_columns)
#         word2vec_columns = ['w2v' + str(i) for i in range(self.size) ]
#         new_columns =  word2vec_columns + old_columns 
#         num_new_columns = len(new_columns)
        
#         # old features
#         old_features = X[old_columns].values
        
#         # features to be returned as np.array
#         features = np.zeros( (num_tokens, num_new_columns), dtype='float32' )

#         # word2vec features as np.array
#         word2vec_features = np.zeros( (num_tokens, self.size), dtype='float32')
        
#         # for loop over all tokens
#         for (i,token) in enumerate(tokens):
#                         if token in self.model.wv:
#                             word2vec_features[i] = self.model.wv[token]
#         features[:, :self.size] = word2vec_features[:]
#         features[:, self.size:] = old_features[:]
#         df = pd.DataFrame(features, index = X.index.copy(), columns=new_columns)
#         df['token'] = tokens
#         self.logger.info('end')
#         return df          

# class GensimWord2VecMultiindex(BaseEstimator, TransformerMixin):    
#     """
#     Gensim word2vec.
#     Transform and fit take as input an series of (token, features).
#     The method transform returns a pd.Series of  list of  (token, features) where 
#     word2vec vectors have been added to features.
#     """
#     def __init__(self, size=200, window=5, mincount=5, sample=1e-3, sg=0, keep_unknown=False, add_features=True):
#         self.size = size
#         self.window = window
#         self.mincount = mincount
#         self.sample = sample
#         self.sg=sg
#         self.keep_unknown=keep_unknown
#         self.add_features = add_features
#         self.logger=logging.getLogger(self.__class__.__name__)

#     def fit(self, X, y=None, token_column = TOKEN):
#         self.logger.info('Start to create `sentences`')   
#         tokens = X.loc[:,token_column ]
#         grouped = tokens.groupby(level=0)
#         sentences = [ list(sentence.values) for _,sentence  in grouped  ]
#         self.logger.info('`sentences` created')
#         self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                               min_count=self.mincount, sample=self.sample, 
#                               sg=self.sg,
#                               workers=2)
#         if self.keep_unknown:
#             tagged_tokens = tokens.where(tokens.apply(self.model.wv.__contains__), TAG_UNKNOWN)
#             index_level = tagged_tokens.index.levels[0]
#             sentences = [ list(tagged_tokens.loc[idx]) for idx in index_level  ]
#             self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                                   min_count=self.mincount, sample=self.sample, 
#                                   sg=self.sg,
#                                   workers=2)
#         self.model.init_sims(replace=True)
#         return self
        
#     def transform(self, X, y=None, token_column = TOKEN):
#         self.logger.info('start transform')
        
#         if not self.add_features:
#             return X        
        
#         tokens = X.loc[:, token_column ]
#         num_tokens = len(tokens)
#         old_columns = list(X.columns.drop(token_column))
#         num_old_columns = len(old_columns)
#         word2vec_columns = ['w2v' + str(i) for i in range(self.size) ]
#         new_columns =  word2vec_columns + old_columns 
#         num_new_columns = len(new_columns)
        
#         # old features
#         old_features = X[old_columns].values
        
#         # features to be returned as np.array
#         features = np.zeros( (num_tokens, num_new_columns), dtype='float32' )

#         # word2vec features as np.array
#         word2vec_features = np.zeros( (num_tokens, self.size), dtype='float32')
        
#         # for loop over all tokens
#         for (i,token) in enumerate(tokens):
#                         if token in self.model.wv:
#                             word2vec_features[i] = self.model.wv[token]
#         features[:, :self.size] = word2vec_features[:]
#         features[:, self.size:] = old_features[:]
#         df = pd.DataFrame(features, index = X.index.copy(), columns=new_columns)
#         df['token'] = tokens
#         self.logger.info('end')
#         return df   
    
    
# class GensimWord2VecMultiindex(BaseEstimator, TransformerMixin):    
#     """
#     Gensim word2vec.
#     Transform and fit take as input an series of (token, features).
#     The method transform returns a pd.Series of  list of  (token, features) where 
#     word2vec vectors have been added to features.
#     """
#     def __init__(self, size=200, window=5, mincount=5, sample=1e-3, sg=0, keep_unknown=False, chuncksize=1000000):
#         self.size = size
#         self.window = window
#         self.mincount = mincount
#         self.sample = sample
#         self.sg=sg
#         self.keep_unknown=keep_unknown
#         self.chuncksize=chuncksize
#         self.logger=logging.getLogger(self.__class__.__name__)

#     def fit(self, X, y=None, token_column = TOKEN):
#         self.logger.info('Start to create `sentences`')   
#         tokens = X.loc[:,token_column ]
#         grouped = tokens.groupby(level=0)
#         sentences = [ list(sentence.values) for _,sentence  in grouped  ]
#         self.logger.info('`sentences` created')
#         self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                               min_count=self.mincount, sample=self.sample, 
#                               sg=self.sg,
#                               workers=2)
#         if self.keep_unknown:
#             tagged_tokens = tokens.where(tokens.apply(self.model.wv.__contains__), TAG_UNKNOWN)
#             index_level = tagged_tokens.index.levels[0]
#             sentences = [ list(tagged_tokens.loc[idx]) for idx in index_level  ]
#             self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                                   min_count=self.mincount, sample=self.sample, 
#                                   sg=self.sg,
#                                   workers=2)
#         self.model.init_sims(replace=True)
#         return self
        
#     def transform(self, X, y=None, token_column = TOKEN):
#         self.logger.info('start transform')
#         tokens = X.loc[:, token_column ]
#         num_tokens = len(tokens)
#         old_columns = list(X.columns.drop(token_column))
#         num_old_columns = len(old_columns)
#         word2vec_columns = ['w2v' + str(i) for i in range(self.size) ]
#         new_columns =  word2vec_columns + old_columns 
#         num_new_columns = len(new_columns)
#         old_features = X[old_columns].values
#         #old_features = X[old_columns]
#         features = np.zeros( (num_tokens, num_new_columns), dtype='float32' )
#         self.logger.info('start chuncksize loop')

#         for i in range(int(num_tokens/self.chuncksize)+1):
#             start_idx = i*self.chuncksize
#             end_idx = min(num_tokens, (i+1)*self.chuncksize)
#             tokens_chunck = tokens.iloc[start_idx:end_idx]
#             self.logger.info('start apply')
#             word2vec_features = np.zeros( (len(tokens_chunck), self.size), dtype='float32')
#             for (i,token) in enumerate(tokens_chunck):
#                 if token in self.model.wv:
#                     word2vec_features[i] = self.model.wv[token]
#             self.logger.info('end apply, create old_features_temp')
#             features[start_idx:end_idx, :self.size] = word2vec_features
#             features[start_idx:end_idx, self.size:] = old_features[start_idx:end_idx]
#             self.logger.info('end features allocate')
#         self.logger.info('end chuncksize loop') 
#         df = pd.DataFrame(features, index = X.index.copy(), columns=new_columns)
#         df['token'] = tokens
#         self.logger.info('end')
#         return df

# # VERSION WITH CHUNCKSIZE
# # THE BETTER SO FAR?
# TAG_UNKNOWN='tag_unknown'
# from gensim.models.word2vec import Word2Vec
# class GensimWord2VecMultiindex(BaseEstimator, TransformerMixin):    
#     """
#     Gensim word2vec.
#     Transform and fit take as input an series of (token, features).
#     The method transform returns a pd.Series of  list of  (token, features) where 
#     word2vec vectors have been added to features.
#     """
#     def __init__(self, size=200, window=5, mincount=5, sample=1e-3, sg=0, keep_unknown=True, chuncksize=1000000):
#         self.size = size
#         self.window = window
#         self.mincount = mincount
#         self.sample = sample
#         self.sg=sg
#         self.keep_unknown=keep_unknown
#         self.chuncksize=chuncksize

#     def fit(self, X, y=None, token_column = TOKEN):
#         tokens = X.loc[:,token_column ].copy()
#         index_level = tokens.index.levels[0]
#         sentences = [ list(tokens.loc[idx]) for idx in index_level  ]
#         self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                               min_count=self.mincount, sample=self.sample, 
#                               sg=self.sg,
#                               workers=2)
#         if self.keep_unknown:
#             tagged_tokens = tokens.where(tokens.apply(self.model.wv.__contains__), TAG_UNKNOWN)
#             index_level = tagged_tokens.index.levels[0]
#             sentences = [ list(tagged_tokens.loc[idx]) for idx in index_level  ]
#             self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                                   min_count=self.mincount, sample=self.sample, 
#                                   sg=self.sg,
#                                   workers=2)
#         self.model.init_sims(replace=True)
#         return self
        
#     def transform(self, X, y=None, token_column = TOKEN):
#         tokens = X.loc[:, token_column ]
#         num_tokens = len(tokens)
#         old_columns = list(X.columns.drop(token_column))
#         num_old_columns = len(old_columns)
#         word2vec_columns = ['w2v' + str(i) for i in range(self.size) ]
#         new_columns = old_columns + word2vec_columns 
#         num_new_columns = len(new_columns)
#         old_features = X[old_columns].values
#         features = np.zeros( (num_tokens, num_new_columns), dtype='float32' )
        
#         for i in range(int(num_tokens/self.chuncksize)+1):
#             #print('step {}'.format(i))
#             start_idx = i*self.chuncksize
#             end_idx = min(num_tokens, (i+1)*self.chuncksize)
#             length = end_idx - start_idx
#             word2vec_features = np.zeros( (length, self.size), dtype='float32' )
#             for i, token in enumerate(tokens.iloc[start_idx:end_idx]):
#                 if token in self.model.wv.vocab:
#                     word2vec_features[i] = self.model.wv[token] 
#                 else:
#                     word2vec_features[i] =  np.zeros(self.size, dtype='float32')
#             old_features_temp = old_features[start_idx:end_idx]
#             concatenated = np.concatenate(( old_features_temp, word2vec_features), axis = 1)
#             features[start_idx:end_idx] = concatenated
            
#         df = pd.DataFrame(features, index = X.index, columns=new_columns)
#         df['token'] = tokens
#         return df
    
# #it works it seems, but slow?
# TAG_UNKNOWN='tag_unknown'
# class GensimWord2VecMultiindexSlow(BaseEstimator, TransformerMixin):
#     import gensim
    
#     """
#     Gensim word2vec.
#     Transform and fit take as input an array like of list of pairs (token, features).
#     The method transform returns a pd.Series of  list of pairs (token, features) where 
#     word2vec vectors have been added to features.
#     """
#     def __init__(self, size=200, window=5, mincount=5, sample=1e-3, sg=0, keep_unknown=True):
#         self.size = size
#         self.window = window
#         self.mincount = mincount
#         self.sample = sample
#         self.sg=sg
#         self.keep_unknown=keep_unknown

#     def fit(self, X, y=None, token_column = TOKEN):
#         tokens = X.loc[:,token_column ].copy()
#         index_level = tokens.index.levels[0]
#         sentences = [ list(tokens.loc[idx]) for idx in index_level  ]
#         self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                               min_count=self.mincount, sample=self.sample, 
#                               sg=self.sg,
#                               workers=2)
#         if self.keep_unknown:
#             tagged_tokens = tokens.where(tokens.apply(self.model.wv.__contains__), TAG_UNKNOWN)
#             index_level = tagged_tokens.index.levels[0]
#             sentences = [ list(tagged_tokens.loc[idx]) for idx in index_level  ]
#             self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
#                                   min_count=self.mincount, sample=self.sample, 
#                                   sg=self.sg,
#                                   workers=2)
#         self.model.init_sims(replace=True)
#         return self
        
#     def transform(self, X, y=None, token_column = TOKEN):
#         tokens = X.loc[:, token_column ]
#         num_tokens = len(tokens)
#         old_columns = X.columns.drop(token_column)
#         num_old_columns = len(old_columns)
#         word2vec_columns = ['w2v' + str(i) for i in range(self.size) ]
#         new_columns = old_columns.union( word2vec_columns ) 
#         num_new_columns = len(new_columns)
#         old_features = X[old_columns].copy()
#         word2vec_features = np.zeros( (num_tokens, self.size), dtype='float32' )
#         decile = int(num_tokens/10)
        
#        # i=0
#         for token in tokens:
#            # if i % decile ==0: print('token {}'.format(i))
#             if token in self.model.wv.vocab:
#                 word2vec_features[i] = self.model.wv[token] 
#                 #df_features.loc[idx] = self.model.wv[token]
#             else:
#                 word2vec_features[i] =  np.zeros(self.size, dtype='float32')
#            # i+=1

#         old_features = X.loc[:,old_columns].values
#         concatenated = np.concatenate(( old_features, word2vec_features), axis = 1)
#         del old_features
#         del word2vec_features
#         df = pd.DataFrame(concatenated, index = X.index, columns=new_columns)
#         df['token'] = tokens
#         return df

# class SeriesOfListOfTokens2MultiindexSeriesOfTokens(BaseEstimator, TransformerMixin):
#     """Transformer to convert pd.Series of lists of tokens into a pd.DataFrome with one single columns 'token' with multiindex: first level is the index of the sentence, second index is the position of the token in the sentence.
    
#     It follows scikit-learn transformer API.
#     """
    
#     def __init__(self):
#         pass
    
#     def fit(self, X, y=None):
#         return self
    
#     def transform(self, X, y=None):
#         dictionary_of_series_of_tokens = {idx: pd.Series(X.loc[idx]) for idx in X.index}
#         multiindex_series_of_tokens = pd.concat(dictionary_of_series_of_tokens, keys=X.index)
#         df = pd.DataFrame({TOKEN: multiindex_series_of_tokens})
#         return df

# class PadderMultiindex(BaseEstimator, TransformerMixin):
#     """Transformer
    
#     Takes the multiindex dataframe of features and returns a 3-d nparray of sequence of length max_len of vectors.
    
#     Parameters
#     ----------
#     max_len : int 
#         Length of the sequence after padding.
    
#     """
#     def __init__(self, max_len, value=0., dtype='float32', add_features=True):
#         self.max_len = max_len
#         self.value=value
#         self.dtype=dtype
#         self.add_features= add_features
#         self.logger = logging.getLogger(self.__class__.__name__)
        
#     def fit(self, X=None, y=None):
#         return self
    
#     def transform(self, X, y=None, token_column = TOKEN):
#         if not self.add_features:
#             return X
#         self.logger.info('start transform')
#         n_samples = len(X.index.levels[0])
#         n_features = X.shape[1]
#         return_array = np.zeros((n_samples, self.max_len, n_features), dtype=self.dtype)
#         i=0
#         for _, tokens_vectorized in X.groupby(level=0):
#                 truncated_length = min(self.max_len, len(tokens_vectorized))
#                 vectors = tokens_vectorized.iloc[:truncated_length]
#                 return_array[i, :truncated_length] = vectors
#                 i+=1
#         self.logger.info('end transform')
#         return return_array