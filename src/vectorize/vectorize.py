from gensim.models.word2vec import Word2Vec
import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from nltk.corpus import words
import regex as re

def featurize_w2v(tokens, word2vec_model):
    """
    tokens is a list of tokens
    word2vec_model is a Gensim word2vec model
    
    Returns a numpay array of shape
    (number of tokens in vocabulary, model.vector_size)
    In case this would be empty, one return a zero array of shape
    (1, model.vector_size)
    """
    # Filter out words that are not in the vocabulary, otherwise word2vec throws an error:
    filtered_tokens = filter(lambda x: x in word2vec_model.wv, tokens)
    featurized_tokens = [word2vec_model[token] for token in filtered_tokens ]
    if len(featurized_tokens)==0:
        featurized_tokens = np.zeros((1,word2vec_model.vector_size))
    else:
        featurized_tokens = np.array(featurized_tokens)
    return featurized_tokens

class GensimWord2Vec(BaseEstimator, TransformerMixin):
    """
    Gensim word2vec.
    Transform and fit take as input an array like of list of tokens.
    The method transform returns a pd.Series of  arrays of the vectors.
    """
    
    def __init__(self, size=200, window=5, mincount=5, sample=1e-3, sg=0):
        self.size = size
        self.window = window
        self.mincount = mincount
        self.sample = sample
        self.sg=sg

    def fit(self, X, y=None):
        self.model = Word2Vec(X, size=self.size, window=self.window, 
                              min_count=self.mincount, sample=self.sample, 
                              sg=self.sg,
                              workers=2)
        self.model.init_sims(replace=True)
        return self
        
    def transform(self, X, y=None):
        X = pd.Series(X)
        Y = X.apply(lambda tokens: featurize_w2v(tokens, self.model))
        return Y

class Averager(BaseEstimator, TransformerMixin):
    
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X = X.apply(lambda x: pd.Series(np.mean(x,axis=0)))
        return X


class Padding(BaseEstimator, TransformerMixin):
    """
    The transform method takes as input X which is a series  with n_samples  lists of vectors (1-D arrays) where the vectors have shape n_features.
    It returns a 3-D arrays of shape (n_samples, maxlen, n_features)
    """
    def __init__(self, maxlen, fill_with=0):
        self.maxlen=maxlen
        self.fill_with=fill_with
        
    def fit(self, X, y=None):
        return self
        
    def transform(self, X, y=None):
        n_samples = len(X)
        assert(X.iloc[0][0].ndim==1)
        n_features = (X.iloc[0])[0].size
        return_array = np.zeros((n_samples, self.maxlen, n_features))
        for i in range(n_samples):
            sequence = X.iloc[i]
            truncated_length = min(self.maxlen,len(sequence))
            sequence = sequence[:truncated_length]
            return_array[i][:truncated_length] = sequence
        return return_array
    
    

###############################################
## CONVERT TOKEN TO PAIR (TOKEN, FEATURE)  ####
## INITIALIZING FETURE TO THE EMPTY LIST   ####
###############################################
class TokenToTokenFeatured(BaseEstimator, TransformerMixin):
    """Transform that converts a list of tokens to list of pairs (token, []) where the list [] will store the features corresponding to token."""
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        """X is a series of list of tokens.
        Returns a series of list of pairs (token, [])
        """
        X = X.apply(lambda tokens: [ (token, []) for token in tokens])
        return X

######################################
## RETRIEVE BACK TOKENS OR FEATURES ##
######################################

class GetFeatures(BaseEstimator, TransformerMixin):
    """Transformer that transform list of (token, features) into features."""
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(lambda tokens_featured: [features for token, features in tokens_featured])
        X = X.apply(np.array)
        return X

class GetToken(BaseEstimator, TransformerMixin):
    """Transformer that transform list of (token, features) into token."""
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(lambda tokens_featured: [token for token, features in tokens_featured])
        return X
    
#################
## LOWER CASE ###
#################
def lower_case_featured(token, features):
    starts_with_upper_case = int(token[0].isupper())
    remaining_upper_case = len(re.findall(r'[A-Z]', token[1:]))
    return (token.lower(), features + [ starts_with_upper_case   , remaining_upper_case  ] )

class LowerCaseFeatured(BaseEstimator, TransformerMixin):
    """Transformer that takes lists of (token, feature) and transform token to lower case, and add to feature 
    one feature indicating if token starts with a capital letter, 
    and adds a second feature counting the remaining capital letters in token.
    """
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(lambda tokens_featured: [lower_case_featured(*token_featured) for token_featured in tokens_featured])
        return X

##########################
## DUPLICATE CHARACTERS ##
##########################
words_set = set(words.words())
regex_duplicate = re.compile(r'(.)\1+')

def remove_duplicate_featured(token, features):
    duplicate_free_word = regex_duplicate.sub(r'\1', token)
    if duplicate_free_word == token:
        return (token, features + [0])
    if token in words_set:
        return (token , features + [0] )
    num_duplicates = len(token)-len(duplicate_free_word)
    return (duplicate_free_word, features + [num_duplicates] )

class RemoveDuplicatesFeatured(BaseEstimator, TransformerMixin):
    """Transformer which takes list of (token, features) and remove consecutive duplicatecharacters in token, and adds a feature to count the number of duplicates."""
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(lambda tokens_featured: [remove_duplicate_featured(*token_featured) for token_featured in tokens_featured])
        return X

    
###########################
### QUOTED TOKEN      #####
###########################
def count_quotes_featured(token, features):
    token1 = re.sub(r"""^['"]+""", "", token)
    if token1!=token:
        token2 = re.sub(r"""['"]+$""", "", token1)
        if token2!=token1:
            return (token2, features + [len(token) - len(token2)])
    return (token, features + [0])
            
class QuotedTokenFeatured(BaseEstimator, TransformerMixin):
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(lambda tokens_featured: [ count_quotes_featured(*token_featured) for token_featured in tokens_featured]  )
        return X
    
    
############################
## TOKEN LENGTH .  #########
############################

class TokenLengthFeatured(BaseEstimator, TransformerMixin):
    """Transformer that takes a list of (token, features) and adds the length of the token to features."""
    def __init_(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X = X.apply(lambda tokens_featured: [ (token, features + [ len(token) ] ) for token, features in tokens_featured]  )
        return X
    
###################################
## TOKEN WITH FEATURE TO GENSIM  ##
###################################

class GensimWord2VecFeatured(BaseEstimator, TransformerMixin):
    """
    Gensim word2vec.
    Transform and fit take as input an array like of list of pairs (token, features).
    The method transform returns a pd.Series of  list of pairs (token, features) where 
    word2vec vectors have been added to features.
    """
    TAG_UNKNOWN='tag_unknown'
    def __init__(self, size=200, window=5, mincount=5, sample=1e-3, sg=0, keep_unknown=False):
        self.size = size
        self.window = window
        self.mincount = mincount
        self.sample = sample
        self.sg=sg
        self.keep_unknown=keep_unknown

    def fit(self, X, y=None):
        sentences =  X.apply(lambda tokens_featured: [token for token, features in tokens_featured])
        self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
                              min_count=self.mincount, sample=self.sample, 
                              sg=self.sg,
                              workers=2)
        if self.keep_unknown:
            sentences = sentences.apply(lambda tokens: token if token in self.model.wv else TAG_UNKNOWN for token in tokens)
            self.model = Word2Vec(sentences=sentences, size=self.size, window=self.window, 
                                  min_count=self.mincount, sample=self.sample, 
                                  sg=self.sg,
                                  workers=2)
        self.model.init_sims(replace=True)
        return self
        
    def transform(self, X, y=None):
        X = pd.Series(X)
        X = X.apply(lambda tokens_featured: [ ( token, features + list(self.model.wv[token])  )
                         for token, features in tokens_featured if token in self.model.wv ]  )
        return X
    
